﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{

    // todo add file system for reading and writing new neetworks and data sets.
    class Program
    {
        static void Main(string[] args)
        {
            NeuralNetwork nn = new NeuralNetwork(4, 3, 4, 4);

            List<TestPair> testPairs = new List<TestPair>();

            string[] files = Directory.GetFiles(@"C:\NNsharp\SimpleNetwork\TrainingData");

            foreach(string file in files)
            {
                List<List<Node>> decouple = SNutils.readTrainingFile(file);
                testPairs.Add(new TestPair(decouple[0], decouple[1]));
            }


            for(int i = 0; i < 100000; i++)
            {
                int r = SNutils.rand.Next(testPairs.Count);
                TestPair tp = testPairs[r];
                nn.trainNetwork(tp.getInputs(), tp.getTargets());
            }


            List<Node> output;
            List<Node> query = new List<Node>();
            query.Add(new Node());
            query.Add(new Node());
            query.Add(new Node());
            query.Add(new Node());
            foreach(Node n in query)
            {
                n.setPassput(0);
            }
            output = nn.queryNetwork(query);

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine();

            foreach (Node n in output)
            {
                Console.WriteLine(n.getPassPut());
            }

            Console.Read();

            // nn.train(data, target);

            // nn.query(data);
            
        }
    }
}
