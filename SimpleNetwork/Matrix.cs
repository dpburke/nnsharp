﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{
   
    /// <summary>
    /// This is a class meant to deal with matrix storage and math, there are many probably beter solutions
    /// but as this is a research project I have opted to do as much by hand as possible, so take these sections
    /// with a grain of salt as they may be incorrect innefficient or lack featuers.
    /// </summary>
    class Matrix
    {
        double[] contents;
        int linearLength { get; }
        /// <summary>
        /// The constructor for the matrix data type.
        /// </summary>
        /// <param name="x">The X size of the matrix</param>
        /// <param name="y">The Y size of the matrix</param>
        public Matrix(int x, int y)
        {
            linearLength = x * y;
            contents = new double[x*y];
            for(int i = 0; i < contents.Length; i++)
            {
                contents[i] = 0;
            }
        }


    }
}
