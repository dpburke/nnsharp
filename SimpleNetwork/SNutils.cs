﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{
    /// <summary>
    /// This is the utility class, it will carry with it all the random function utilities needed
    /// </summary>
    static class SNutils
    {
        public static int IDcount = 0;
        /// <summary>
        /// this is the sigmoid activation function, the function itself is 1/(1+exp^-x)
        /// </summary>
        /// <param name="d">The number to be run through the function</param>
        /// <returns></returns>
        public static double sigmoid(double d)
        {
            return 1 / (1 + Math.Exp(-d));
        }

        public static List<List<Node>> readTrainingFile(String fileName)
        {
            int mode = 0;
            // 0 = start mode 1 = inputs mode 2 = targets mode
            List<List<Node>> output = new List<List<Node>>();
            List<Node> inputs = new List<Node>();
            List<Node> targets = new List<Node>();
            List<string> readin = new List<string>();
            string line = "";
            string ext = Path.GetExtension(fileName);
            if(!ext.Contains("ctrn"))
            {
                Console.WriteLine("Wrong filetype detected \n returning null");
                return null;
            }

            System.IO.StreamReader sr = new System.IO.StreamReader(fileName);  

            while((line = sr.ReadLine()) != null)
            {
                readin.Add(line);
                Console.WriteLine(line);
            }
            bool skip = true;
            foreach (string s in readin)
            {
                if (s.Contains("inputs"))
                {
                    mode = 1;
                    string inputlayer = s.Split(':').Last();
                    Console.WriteLine(inputlayer);
                    skip = true;

                }
                else if (s.Contains("targets"))
                {
                    mode = 2;
                    string targetlayer = s.Split(':').Last();
                    Console.WriteLine(targetlayer);
                    skip = true;
                }
                if (mode == 1 && !skip)
                {
                    Node n = new Node();
                    n.setPassput(Convert.ToDouble(s));
                    inputs.Add(n);

                }
                else if (mode == 2 && !skip)
                {
                    Node n = new Node();
                    n.setPassput(Convert.ToDouble(s));
                    targets.Add(n);
                }

                skip = false;

            }
            sr.Close();

            output.Add(inputs);
            output.Add(targets);

            return output;

        }

        
        static TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
        static int secondsSinceEpoch = (int)t.TotalSeconds;
        public static Random rand = new Random(secondsSinceEpoch);
    }
}
