using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleNetwork { 
    class TestPair
    {
        List<Node> inputs;
        List<Node> targets;

        public TestPair(List<Node> isn, List<Node> tsn)
        {
            inputs = isn;
            targets = tsn;
        }

        public List<Node> getInputs()
        {
            return inputs;
        }
        public void setInputs(List<Node> ns)
        {
            inputs = ns;
        }
        public List<Node> getTargets()
        {
            return targets;
        }
        public void setTargets(List<Node> ns)
        {
            targets = ns;
        }
    }
}