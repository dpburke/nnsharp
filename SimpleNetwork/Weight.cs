﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{
    /// <summary>
    /// Just a place holder for the weight probably ineff maybe refactor this out later
    /// </summary>
    class Weight
    {
        double lWeight;

        Node assNode; 

        public Weight(double d, Node n)
        {
            lWeight = d;
            
            assNode = n;
        }

        public double getWeightedPassput()
        {
            return (assNode.getPassPut() * lWeight);
        }

        public double getLweight()
        {
            return lWeight;
        }

        public void setLweight(double d)
        {
            lWeight = d;
        }

        public Node getAssNode()
        {
            return assNode;
        }
    }
}
