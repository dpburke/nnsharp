﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{
    /// <summary>
    /// This is a layer and will hold all the nodes in a layer
    /// </summary>
    class Layer
    {

        double bias { get; set; }
        List<Node> contents = new List<Node>();
        Layer nextLayer;
        /// <summary>
        /// Creates a new layer 
        /// </summary>
        /// <param name="nodes">the amount of nodes to put in the layer</param>
        /// <param name="prevLayer">The previos layer in the network</param>
        public Layer(int nodes, Layer prevLayer)
        {
            bias = 1;
            for (int i = 0; i < nodes; i++)
            {
                contents.Add(new Node(prevLayer));               
            }
        }

        public Layer(int nodes)
        {
            bias = 1;
            for(int i = 0; i < nodes; i++)
            {
                contents.Add(new Node());
            }
        }
        /// <summary>
        /// adds a node on to the layer in a new location
        /// </summary>
        /// <param name="n">The node to be added to the layer</param>
        public void pushNode(Node n ) 
        {
            contents.Add(n);
        }

        public void setNextLayer(Layer ns)
        {
            nextLayer = ns;
            foreach(Node n in contents)
            {
                n.setNextNodes(ns);
            }
        }

        public List<Node> getContents()
        {
            return contents;
        }

        public int nodeCount()
        {
            return contents.Count;
        }
    }
}
