﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 

namespace SimpleNetwork
{
    /// <summary>
    /// this is the basic Node for the network and will be the basic building block for the network
    /// </summary>
    class Node
    {
        Layer previousNodes;
        List<Weight> prevWeights = new List<Weight>();
        Layer nextNodes;
        public double passput { set; get; }
        public double passback;
        int id;
        /// <summary>
        /// The constructor for the Node class
        /// </summary>
        /// <param name="ns">The list of nodes in the previous layer of the network</param>
        public Node(Layer ns)
        {
            id = SNutils.IDcount;
            SNutils.IDcount++;
            

            previousNodes = ns;
            foreach(Node n in previousNodes.getContents())
            {
                prevWeights.Add(new Weight(SNutils.rand.NextDouble(), n));
            }
            
        }
        public Node()
        {

        }

        public double addPrevWeight()
        {
            double output = 0;
            
            foreach(Weight w in prevWeights)
            {
                output += w.getWeightedPassput();
            }
            return output;
        }

        public double rawTotalWeight()
        {
            double output = 0;
            foreach(Weight w in prevWeights)
            {
                output += w.getLweight();
            }
            return output;
        }

        public void setPassput(double d)
        {
            passput = d;
        }
        public double getPassPut()
        {
            return passput;
        }

        public void setPassback(double d)
        {
            passback = d;
        }

        public double getPassback()
        {
            return passback;
        }  

        public void setNextNodes(Layer ns)
        {
            nextNodes = ns;
        }

        public List<Weight> getWeights()
        {
            return prevWeights;
        }

    }
}
