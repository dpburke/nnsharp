﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleNetwork
{
    /// <summary>
    /// this is a complete network and will encapsulate all of the functionality of the network
    /// </summary>
    class NeuralNetwork
    {
        double learningRate = 1;
        Layer inputLayer;
        List<Layer> hiddenLayers;
        Layer outputLayer;

        /// <summary>
        /// Create a NeuralNetwork defining the different aspects of the network
        /// </summary>
        /// <param name="layers">The amount of hidden layers</param>
        /// <param name="nodes">The amount of nodes to default in each hidden layer</param>
        /// <param name="outputs">The amount of output nodes</param>
        /// <param name="inputs">The amount of input nodes</param>
        public NeuralNetwork(int inputs, int layers, int nodes, int outputs)
        {
            

            hiddenLayers = new List<Layer>();

            inputLayer = new Layer(inputs);

            Layer lastLayer = inputLayer;

            for (int i = 0; i<layers; i++)
            {
                //create a new layer referencing the last layer
                Layer l = new Layer(nodes,lastLayer);

                // set the last layer to reference he next layer
                lastLayer.setNextLayer(l);

                // set the last layer to the current letter
                lastLayer = l;
                
                // push the new layer on to the collection
                hiddenLayers.Add(l);
            }
            outputLayer = new Layer(outputs, lastLayer);
        }

        /// <summary>
        /// Query the network for a result by feeding forward without checking against any target data
        /// </summary>
        /// <param name="inputs">The prepared data to be sent through the network</param>
        /// <returns>returns a list of the output nodes</returns>
        public List<Node> queryNetwork(List<Node> inputs)
        {
            if(inputs.Count != inputLayer.getContents().Count)
            {
                Console.WriteLine("missmatching input and query size");
                return null;
            }

            //List<Node> output = new List<Node>();
            
            for(int i = 0; i < inputs.Count; i++)
            {
                inputLayer.getContents()[i].setPassput(inputs[i].getPassPut());
            }

            //Layer interumHolder = inputLayer;

            foreach(Layer l in hiddenLayers)
            {
                foreach(Node n in l.getContents())
                {
                    double s = 0;
                    s = SNutils.sigmoid(n.addPrevWeight());
                    n.setPassput(s);
                }
                
            }

            foreach(Node n in outputLayer.getContents())
            {
                double s = 0;
                s = SNutils.sigmoid(n.addPrevWeight());
                n.setPassput(s);
            }

            return outputLayer.getContents();
        }

        /// <summary>
        /// Trains the network based on input and linked target data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="targets"></param>
        public void trainNetwork(List<Node> input, List<Node> targets)
        {
            List<Node> temp;

            temp = queryNetwork(input);

            List<double> errors = new List<double>();

            for(int i = 0; i < temp.Count; i++)
            {
                 errors.Add(temp[i].getPassPut() - targets[i].getPassPut());
            }
            List<Node> outHolder = outputLayer.getContents();
            
            for (int i = 0; i < errors.Count(); i++)
            {
                foreach(Weight w in outHolder[i].getWeights())
                {
                    double rawTotal = outHolder[i].rawTotalWeight();
                    double lWeight = w.getLweight();
                    double errorSplit = (lWeight / rawTotal);
                    double thisError = errors[i];
                    double weightedError = 
                        -(
                        learningRate
                        * thisError
                        * errorSplit);

                    double newWeight = lWeight + weightedError;
                    w.setLweight(newWeight);

                   

                    w.getAssNode().setPassback(weightedError);
                }
            }
           
            for(int i = hiddenLayers.Count()-1; i > 0; i--)
            {
                foreach(Node n in hiddenLayers[i].getContents())
                {
                    foreach(Weight w in n.getWeights())
                    {
                        w.setLweight(w.getLweight() 
                            + (learningRate 
                            * n.getPassback()
                            *(w.getLweight()/n.rawTotalWeight())));
                    }
                }
            }


           
        }

        public override string ToString()
        {
            string output = "";
            output +="Input Nodes: " + inputLayer.nodeCount() + "\n";
            foreach(Layer l in hiddenLayers)
            {
                output +="hidden layer: " + l.nodeCount() + "\n";
            }
            output += "output Nodes:" + outputLayer.nodeCount() + "\n";

            return output;
        }

    }
}
